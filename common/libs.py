import pypandoc
import datetime
import os
from selenium import webdriver


def is_element_exist_by_id(driver, idt: str):
    try:
        driver.find_element_by_id(idt)
        return True
    except Exception as e:
        print(e)
        return False


def is_in_time(date: datetime.date, start: datetime.date, end: datetime.date):
    return start <= date <= end


def format_datetime(date: str):
    arr = date.split('-', 2)
    if len(arr) < 3:
        return None
    return datetime.date(int(arr[0]), int(arr[1]), int(arr[2]))


def create_html(content, file_path):
    f = open(file_path, 'w', encoding='utf-8')
    f.write(content)
    f.close()


def transform(src, dst):
    pypandoc.convert_file(src, 'docx', format='html', outputfile=dst)


def remove_html(file_path: str):
    os.remove(file_path)


def server_driver(exp={}):
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')

    for key, value in exp.items():
        options.add_experimental_option(key, value)
    driver = webdriver.Chrome(options=options)
    return driver
