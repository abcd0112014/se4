#!/usr/bin/env bash

set -e

PATH=${PATH}:/usr/bin:/sbin:~/.local/bin

# 安装目录
deploy_path=.

export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8

pip3 install --user virtualenv  --index-url http://mirrors.tencent.com/pypi/simple --trusted-host mirrors.tencent.com

cd ${deploy_path}
venv_name="venv"
virtualenv ${venv_name} -p python3
${venv_name}/bin/pip3 install -r requirements.txt

echo "venv is ready"
rm -rf data
mkdir data
cd data
mkdir department law other rule
echo "init succeed"
