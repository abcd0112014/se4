from wtforms.validators import *
from wtforms import Form
from wtforms.fields import *
import wtforms_json
wtforms_json.init()


class SpiderForm(Form):
    start = StringField(validators=[DataRequired()])
    end = StringField(validators=[DataRequired()])
    mode = SelectField(choices=[('all', 'all'),
                                ('law', 'law'),
                                ('rule', 'rule'),
                                ('department', 'department'),
                                ('other', 'other')],
                       default='all')
