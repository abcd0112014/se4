from flask import Flask, request
import threading
from pbc_gov_cn import PbcGovCn
from form.forms import SpiderForm
app = Flask(__name__)


@app.route('/', methods=['POST'])
def spider():
    form = SpiderForm.from_json(request.json)
    pbc_gov_cn = PbcGovCn(form)
    thread = threading.Thread(target=pbc_gov_cn.execute)
    thread.start()
    return "start spider successfully!"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8888, debug=True)
