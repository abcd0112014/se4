#!/usr/bin/env python 
# -*- coding:utf-8 -*-

import os

from common.libs import *
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
PREFIX = os.path.join(os.getcwd(), 'data')


class PbcGovCn:
    def __init__(self, form):
        self.urls = [
            "http://www.pbc.gov.cn/tiaofasi/144941/144951/index.html",  # law
            "http://www.pbc.gov.cn/tiaofasi/144941/144957/index.html",  # department
            "http://www.pbc.gov.cn/tiaofasi/144941/3581332/index.html",  # rule
            "http://www.pbc.gov.cn/tiaofasi/144941/144959/index.html"  # other
        ]
        self.dirs = [
            "law", "department", "rule", "other"
        ]
        self.start = format_datetime(form.start.data)
        self.end = format_datetime(form.end.data)
        self.mode = form.mode.data

    def execute(self):
        mode = self.mode
        urls = self.urls
        dirs = self.dirs
        if mode == "all":
            for _index in range(len(urls)):
                print("-----%s-----" % dirs[_index])
                self._stub_page(urls[_index], dirs[_index])
        else:
            print("-----%s-----" % mode)
            self._stub_page(urls[dirs.index(mode)], mode)

    def _stub_page(self, begin_url: str, dst: str):
        driver = server_driver()
        driver.get(begin_url)
        while True:
            flag = self._get_page_content(driver, dst)
            if not flag:
                break
            res = self._get_next_page(driver)
            if not res[1]:
                break
            res[0].click()
        driver.quit()

    def _get_page_content(self, driver: WebDriver, dst: str):
        elements = driver.find_elements_by_xpath(
            "//div[@id='r_con']/div/div[2]/div/table/tbody/tr[2]/td/table/tbody/tr/td[2]")
        for item in elements:
            inner = item.find_element_by_xpath(".//a")
            date = item.find_element_by_xpath(".//span")
            res = format_datetime(date.text)
            if res:
                if is_in_time(res, self.start, self.end):
                    print("%s %s" % (inner.text, date.text))
                    self._download_per_content(inner, dst, inner.text)
                else:
                    return False
        return True

    def _get_next_page(self, driver: WebDriver):
        """
        get next page content
        :param driver:
        :return next_page: next page WebElement
        :return is_next: has next page or not
        """
        next_page = driver.find_element_by_xpath(".//a[text()='下一页']")
        is_next = next_page.get_attribute("tagname") != "[NEXTPAGE]"
        return next_page, is_next

    def _download_per_content(self, item: WebElement, dst: str, title: str):
        prefs = {
            'profile.default_content_settings.popups': 0,
            'download.default_directory': os.path.join(PREFIX, dst)
        }
        driver = server_driver({'prefs': prefs})
        driver.implicitly_wait(2)
        driver.get(item.get_attribute('href'))

        # 假设无直接文件下载，则处理页面生成文件
        if is_element_exist_by_id(driver, 'zoom'):
            element = driver.find_element_by_id('zoom')
            content_text = element.get_attribute('innerHTML')
            src = os.path.join(PREFIX, dst, title + '.html')
            dst = os.path.join(PREFIX, dst, title + '.docx')
            create_html(content_text, src)
            transform(src, dst)
            remove_html(src)
        driver.quit()


if __name__ == '__main__':
    pbc_gov_cn = PbcGovCn(start_time=datetime.date(2021, 4, 10),
                          end_time=datetime.date(2021, 8, 31),
                          mode="rule")
    pbc_gov_cn.execute()