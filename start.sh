#!/usr/bin/env bash
set -e
if ps -ef|grep uwsgi|grep -v grep; then
  ps -ef|grep uwsgi|grep -v grep|awk '{print $2}'|xargs kill -9 || echo
fi
cd /root/weiyihui/se4
source venv/bin/activate
uwsgi uwsgi.ini
